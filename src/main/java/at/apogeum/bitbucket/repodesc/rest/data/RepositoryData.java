package at.apogeum.bitbucket.repodesc.rest.data;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;

/**
 * @author Stephan Bechter <stephan@apogeum.at>
 */
@XmlAccessorType(XmlAccessType.FIELD)
public class RepositoryData {

    @XmlElement
    private String name;
    @XmlElement
    private String description;
    @XmlElement
    private String descriptionFull;
    @XmlElement
    private String lastUpdateTime;
    @XmlElement
    private String lastUpdateMessage;
    @XmlElement
    private String lastUpdateMessageFull;
    @XmlElement
    private String lastAuthor;

    public RepositoryData() {
    }

    public RepositoryData(
            String name, String description, String lastUpdateTime,
            String lastUpdateMessage, String lastUpdateMessageFull, String lastAuthor
    ) {
        this.name = name;
        this.descriptionFull = description;
        if (description.length() < 85) {
            this.description = description;
        } else {
            this.description = description.substring(0, 80) + "...";
        }
        this.lastUpdateTime = lastUpdateTime;
        this.lastUpdateMessageFull = lastUpdateMessageFull;
        if (lastUpdateMessage.length() < 85) {
            this.lastUpdateMessage = lastUpdateMessage;
        } else {
            this.lastUpdateMessage = lastUpdateMessage.substring(0, 80) + "...";
        }
        String n = lastAuthor;
        int x = n.indexOf('<');
        if (x >= 0) {
            this.lastAuthor = n.substring(0, x).trim();
        } else {
            this.lastAuthor = n;
        }

        this.description = safeForHtml(this.description);
        this.descriptionFull = safeForHtml(this.descriptionFull);
        this.lastUpdateMessage = safeForHtml(this.lastUpdateMessage);
        this.lastUpdateMessageFull = safeForHtml(this.lastUpdateMessageFull);
    }

    private static String safeForHtml(String s) {
        s = s.replace("&", "&amp;");
        s = s.replace("<", "&lt;");
        s = s.replace(">", "&gt;");
        s = s.replace("'", "&apos;");
        return s;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getDescriptionFull() {
        return descriptionFull;
    }

    public void setDescriptionFull(String descriptionFull) {
        this.descriptionFull = descriptionFull;
    }

    public String getLastUpdateTime() {
        return lastUpdateTime;
    }

    public void setLastUpdateTime(String lastUpdateTime) {
        this.lastUpdateTime = lastUpdateTime;
    }

    public String getLastUpdateMessage() {
        return lastUpdateMessage;
    }

    public void setLastUpdateMessage(String lastUpdateMessage) {
        this.lastUpdateMessage = lastUpdateMessage;
    }

    public String getLastUpdateMessageFull() {
        return lastUpdateMessageFull;
    }

    public void setLastUpdateMessageFull(String lastUpdateMessageFull) {
        this.lastUpdateMessageFull = lastUpdateMessageFull;
    }

    public String getLastAuthor() {
        return lastAuthor;
    }

    public void setLastAuthor(String lastAuthor) {
        this.lastAuthor = lastAuthor;
    }

}
