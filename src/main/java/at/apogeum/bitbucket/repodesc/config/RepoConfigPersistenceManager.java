package at.apogeum.bitbucket.repodesc.config;

import at.apogeum.bitbucket.repodesc.managers.RepositoryCallback;
import at.apogeum.bitbucket.repodesc.managers.RepositoryManager;
import com.atlassian.activeobjects.external.ActiveObjects;
import com.atlassian.bitbucket.repository.Repository;
import net.java.ao.DBParam;
import net.java.ao.Query;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/**
 * @author Stephan Bechter <stephan@apogeum.at>
 */
public class RepoConfigPersistenceManager {

    private static final Logger LOG = LoggerFactory.getLogger(RepoConfigPersistenceManager.class);

    private final ActiveObjects activeObjects;
    private final RepositoryManager repositoryManager;

    public RepoConfigPersistenceManager(ActiveObjects activeObjects, RepositoryManager repositoryManager) {
        this.activeObjects = activeObjects;
        this.repositoryManager = repositoryManager;
    }

    /**
     * Get the configuration for a Repository
     * @param repository the repository
     * @return the configuration. If no config found create a new one with default values
     * @throws java.sql.SQLException on sql error
     */
    public RepoConfig getRepositoryConfigurationForRepository(final Repository repository) throws SQLException {
        RepoConfig[] repos = activeObjects.find(RepoConfig.class,
            Query.select().where("REPO_ID = ?", repository.getId()));
        if (repos.length == 0) {
            // just use the defaults
            RepoConfig config = activeObjects.create(
                            RepoConfig.class,
                            new DBParam("REPO_ID", repository.getId()));
            config.setDescription("");
            config.save();
            return config;
        }
        return repos[0];
    }

    public List<RepoConfig> getRepositoryConfigurationsForProjectKey(final String projectKey) throws SQLException {
        final List<RepoConfig> result = new ArrayList<RepoConfig>();

        repositoryManager.findByProjectKey(projectKey, new RepositoryCallback() {
            @Override
            public void handle(Repository repository) {
                RepoConfig config = null;
                try {
                    config = getRepositoryConfigurationForRepository(repository);
                } catch (SQLException e) {
                    throw new IllegalStateException("Error getting repository config", e);
                }
                result.add(config);

            }
        });
        return result;
    }

    public List<RepoConfig> getRepositoryConfigurationsForPublicProjects() throws SQLException {
        final List<RepoConfig> result = new ArrayList<RepoConfig>();

        repositoryManager.findPublic(new RepositoryCallback() {
            @Override
            public void handle(Repository repository) {
                RepoConfig config = null;
                try {
                    config = getRepositoryConfigurationForRepository(repository);
                } catch (SQLException e) {
                    throw new IllegalStateException("Error getting repository config", e);
                }
                result.add(config);
            }
        });
        Collections.sort(result, new Comparator<RepoConfig>() {
            @Override
            public int compare(RepoConfig o1, RepoConfig o2) {
                String o1Sort = o1.getProjectKey() + o1.getSlug();
                String o2Sort = o2.getProjectKey() + o2.getSlug();
                return o1Sort.compareTo(o2Sort);
            }
        });
        return result;
    }

    public List<RepoConfig> getRepositoryConfigurationsForPrivateProjects(final String userId) throws SQLException {
        final List<RepoConfig> result = new ArrayList<RepoConfig>();
        repositoryManager.findPrivate(userId, new RepositoryCallback() {
            @Override
            public void handle(Repository repository) {
                RepoConfig config = null;
                try {
                    config = getRepositoryConfigurationForRepository(repository);
                } catch (SQLException e) {
                    throw new IllegalStateException("Error getting repository config", e);
                }
                result.add(config);
            }
        });
        return result;
    }

    public void setRepositoryConfigurationForRepository(final Repository repo, final String description)
                    throws SQLException, IllegalArgumentException {
        RepoConfig[] repos = activeObjects.find(
                        RepoConfig.class,
                        Query.select().where("REPO_ID = ?", repo.getId()));
        if (repos.length == 0) {
            if (LOG.isInfoEnabled()) {
                LOG.info("Creating repository configuration for id: " + repo.getId());
            }

            RepoConfig config = activeObjects.create(
                            RepoConfig.class,
                            new DBParam("REPO_ID", repo.getId()),
                            new DBParam("PROJECT_KEY", repo.getProject().getKey()),
                            new DBParam("SLUG", repo.getSlug()),
                            new DBParam("DESCRIPTION", description));
            config.save();
            return;
        }
        RepoConfig foundRepo = repos[0];
        foundRepo.setDescription(description);
        foundRepo.setSlug(repo.getSlug());
        foundRepo.setProjectKey(repo.getProject().getKey());
        foundRepo.save();
    }
}
