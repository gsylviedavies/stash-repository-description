package at.apogeum.bitbucket.repodesc.managers;

import com.atlassian.bitbucket.repository.Repository;
import com.atlassian.bitbucket.repository.RepositoryService;
import com.atlassian.bitbucket.util.Page;
import com.atlassian.bitbucket.util.PageRequest;
import com.atlassian.bitbucket.util.PageRequestImpl;
import org.apache.commons.lang3.StringUtils;

/**
 * @author Stephan Bechter <stephan@apogeum.at>
 */
public class RepositoryManager {

    private final RepositoryService repositoryService;

    private int pageSize = 100;

    public RepositoryManager(RepositoryService repositoryService) {
        this.repositoryService = repositoryService;
    }

    public void findByProjectKey(String projectKey, RepositoryCallback callback) {
        PageRequest pageRequest = new PageRequestImpl(0, pageSize);
        Page<? extends Repository> repositories = repositoryService.findByProjectKey(projectKey, pageRequest);
        while(true) {
            for (Repository repository : repositories.getValues()) {
                callback.handle(repository);
            }
            if (repositories.getIsLastPage()) {
                break;
            }
            repositories = repositoryService.findByProjectKey(projectKey, repositories.getNextPageRequest());
        }
    }

    public void findPublic(RepositoryCallback callback) {
        PageRequest pageRequest = new PageRequestImpl(0, pageSize);
        Page<? extends Repository> allRepos = repositoryService.findAll(pageRequest);
        while(true) {
            for (Repository repository : allRepos.getValues()) {
                if (repository.isPublic()) {
                    callback.handle(repository);
                }
            }
            if (allRepos.getIsLastPage()) {
                break;
            }
            allRepos = repositoryService.findAll(allRepos.getNextPageRequest());
        }
    }

    public void findPrivate(String userId, RepositoryCallback callback) {
        PageRequest pageRequest = new PageRequestImpl(0, pageSize);
        Page<? extends Repository> allRepos = repositoryService.findAll(pageRequest);
        while(true) {
            for (Repository repository : allRepos.getValues()) {
                if (StringUtils.equals("~" + userId.toUpperCase(), repository.getProject().getKey())) {
                    callback.handle(repository);
                }
            }
            if (allRepos.getIsLastPage()) {
                break;
            }
            allRepos = repositoryService.findAll(allRepos.getNextPageRequest());
        }
    }

    public int getPageSize() {
        return pageSize;
    }

    public void setPageSize(int pageSize) {
        this.pageSize = pageSize;
    }
}
